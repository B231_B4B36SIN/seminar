package com.cmbk.crud.employee.services;

import com.cmbk.crud.employee.domain.CreateEmployeeRequest;
import com.cmbk.crud.employee.domain.CreateEmployeeResponse;
import com.cmbk.crud.employee.domain.EmployeeResponse;

/**
 * @author chanaka.k
 *
 */
public interface EmployeeService {

	/**
	 * Save new Employee object
	 *
	 */
	public CreateEmployeeResponse createEmployee(CreateEmployeeRequest createEmployeeRequest);

	/**
	 * Retrieve Employee object by Id
	 *
	 */
	public EmployeeResponse retrieveEmployeeById(String employeeID);

}
