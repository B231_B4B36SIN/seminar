package com.cmbk.crud.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.cmbk.crud.employee" })
public class SpringBasicAuthenticationAppBlogApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBasicAuthenticationAppBlogApplication.class, args);
	}

}
