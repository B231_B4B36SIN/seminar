package com.cmbk.crud.employee.services;

import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import com.cmbk.crud.employee.domain.CreateEmployeeRequest;
import com.cmbk.crud.employee.domain.CreateEmployeeResponse;
import com.cmbk.crud.employee.domain.Employee;
import com.cmbk.crud.employee.domain.EmployeeResponse;
import com.cmbk.crud.employee.domain.common.exception.NotFoundException;
import com.cmbk.crud.employee.domain.transform.EmployeeTransformer;
import com.cmbk.crud.employee.repositories.EmployeeRepository;

/**
 * @author chanaka.k
 *
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

	final static Logger logger = Logger.getLogger(EmployeeServiceImpl.class);

	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public CreateEmployeeResponse createEmployee(CreateEmployeeRequest createEmployeeRequest) {

		logger.info("Inside create employee service");

		Employee employee = null;
		CreateEmployeeResponse response = null;

		try {

			employee = EmployeeTransformer.transformEmployeeRequestToDto(createEmployeeRequest);
			employee = employeeRepository.save(employee);
			response = EmployeeTransformer.transformEmployeeDtoToResponse(employee);

		} catch (HttpServerErrorException hse) {
			logger.error("Server Error found : " + hse.getMessage().toString());
			hse.printStackTrace();
		} catch (Exception e) {
			logger.error("Error found : " + e.getMessage().toString());
			e.printStackTrace();
		}

		logger.info("Successfully saved new employee");

		return response;
	}

	@Override
	public EmployeeResponse retrieveEmployeeById(String employeeId) {

		logger.info("Inside retrieve employee by id, service");

		Optional<Employee> optionalEmployee = null;
		Employee employee = null;
		EmployeeResponse employeeResponse = null;

		optionalEmployee = employeeRepository.findById(employeeId);

		if (optionalEmployee.isPresent()) {
			employee = optionalEmployee.get();
		} else {
			throw new NotFoundException("INVALID_EMPLOYEE_ID", "Employee not found for the given id");
		}

		employeeResponse = EmployeeTransformer.employeeToEmployeeResponse(employee);

		logger.info("Successfully returned employee by id");
		return employeeResponse;
	}

}
