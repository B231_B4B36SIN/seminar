package com.cmbk.crud.employee.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.cmbk.crud.employee.domain.Employee;

/**
 * @author chanaka.k
 *
 */

@Repository
public interface EmployeeRepository extends MongoRepository<Employee, String> {

}
